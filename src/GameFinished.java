import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameFinished extends JFrame {
    private static JDialog dialog;

    public GameFinished() {
        dialog = new JDialog(this, "Glückwunsch! Du hast alle 200 Reihen aufgelöst!", true);

        // Initialize components
        JLabel label = new JLabel("Game over!");
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        setLayout(new FlowLayout());
        // Add components
        add(label); add(button);

        setSize(300,150);
        setVisible(true);
    }
}
