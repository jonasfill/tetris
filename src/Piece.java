import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Piece {

    private PieceType pieceType;

    private PieceAlignment pieceAlignment;

    // Position in grid (without padding)
    private Point position;

    public Piece(PieceType pieceType, PieceAlignment pieceAlignment, Point position) {
        this.pieceType = pieceType;
        this.pieceAlignment = pieceAlignment;
        this.position = position;
    }

    public Piece() {
        // Chooses random piece type
        Random random = new Random();
        Random randomInner; int randInner;
        int rand = random.nextInt(5);
        switch (rand) {
            case 0:
                this.pieceType = PieceType.BAR;
                break;
            case 1:
                this.pieceType = PieceType.SQUARE;
                break;
            case 2:
                this.pieceType = PieceType.DROMEDAR;
                break;
            case 3:
                // Further random generator to choose between right and left aligned skew
                randomInner = new Random();
                randInner = randomInner.nextInt(2);
                switch (randInner) {
                    case 0:
                        this.pieceType = PieceType.SKEW_RIGHT;
                        break;
                    case 1:
                        this.pieceType = PieceType.SKEW_LEFT;
                        break;
                }
                break;
            case 4:
                // Further random generator to choose between right and left aligned lshape
                randomInner = new Random();
                randInner = randomInner.nextInt(2);
                switch (randInner) {
                    case 0:
                        this.pieceType = PieceType.LSHAPE_RIGHT;
                        break;
                    case 1:
                        this.pieceType = PieceType.LSHAPE_LEFT;
                        break;
                }
                break;
        }
        // Choose random position
        this.position = new Point(0,Grid.getHEIGHT() / Grid.getBlockWidth() + 5);
        if (this.pieceType == PieceType.DROMEDAR || this.pieceType == PieceType.SKEW_RIGHT||
                this.pieceType == PieceType.SKEW_LEFT || this.pieceType == PieceType.LSHAPE_RIGHT ||
                this.pieceType == PieceType.LSHAPE_LEFT) {
            // TODO: Set x to value between 1 and 18 -> ex. if 20 blocks BETTER!
            random = new Random();
            // Between 0 and 18
            rand = random.nextInt(Grid.getWIDTH() / Grid.getBlockWidth() - 2);
            rand++;
            this.position.x = rand;
        }
        else if (this.pieceType == PieceType.SQUARE) {
            // Set x to value between 0 and 18
            random = new Random();
            rand = random.nextInt(Grid.getWIDTH() / Grid.getBlockWidth() - 1);
            this.position.x = rand;
        }
        else if (this.pieceType == PieceType.BAR) {
            // TODO: Set x to value between 1 and 17
            random = new Random();
            // Between 0 and 16
            rand = random.nextInt(Grid.getWIDTH() / Grid.getBlockWidth() - 3);
            rand++;
            this.position.x = rand;
        }
        /*
        this.pieceType = PieceType.SQUARE;
        this.position = new Point(5,Grid.getHEIGHT() / Grid.getBlockWidth() + 5);
        */
        // Set piece alignment to default
        this.pieceAlignment = PieceAlignment.VERTICAL_RIGHT;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public void setPieceType(PieceType pieceType) {
        this.pieceType = pieceType;
    }

    public PieceAlignment getPieceAlignment() {
        return pieceAlignment;
    }

    public void setPieceAlignment(PieceAlignment pieceAlignment) {
        this.pieceAlignment = pieceAlignment;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void down() {
        position.y--;
    }
}
