import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WelcomePage extends JFrame implements ActionListener {

    private static Color TEXT_COLOR = Color.BLUE;

    private static Font LABEL_FONT = new Font("Courier", Font.BOLD, 25);

    private static Font BUTTON_FONT = new Font("Courier", Font.PLAIN, 15);

    private JLabel welcomePanel;

    private JButton newGame;

    public WelcomePage() {
        this.setVisible(true);
        this.setSize(new Dimension(420, 400));

        this.setLayout(new FlowLayout());
        this.welcomePanel = new JLabel("Welcome to Tetris");
        this.welcomePanel.setForeground(TEXT_COLOR);
        this.welcomePanel.setFont(LABEL_FONT);
        this.welcomePanel.setPreferredSize(new Dimension(420, 400/2));
        this.welcomePanel.setHorizontalAlignment(SwingConstants.CENTER);

        this.newGame = new JButton("New game");
        this.newGame.setForeground(TEXT_COLOR);
        this.newGame.setFont(BUTTON_FONT);
        this.newGame.addActionListener(this);

        this.add(this.welcomePanel);
        this.add(this.newGame);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.setVisible(false);
        new Game();
    }
}
