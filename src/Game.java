import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Game extends JFrame implements ActionListener, KeyListener {

    private Grid grid;

    private InfoLabel infoLabel;

    // Refreshing rate
    private static int[] FREQUENCY = {400, 360, 320, 280, 240, 200, 160, 140, 120, 100,
    90, 80, 70, 60, 50, 40, 30, 20, 10, 1};

    // Regulates interrupts
    private int counter = 0;

    // Keeps track of score
    private static int SCORE = 0;

    // Current level
    private static int LEVEL = 1;

    // Time for the player to edit the piece when its already on the ground
    private static int[] SPIEL = {3, 4, 5, 5, 5, 5, 6, 7, 7, 7, 8, 8, 9, 9, 10, 20, 20, 40, 80, 1500};
    private int spielCounter = 0;

    private Timer timer;

    public Game() {
        this.setVisible(true);
        this.setSize(new Dimension(420, 900));

        // add compoenents
        this.grid = new Grid();
        this.add(grid);
        this.infoLabel = new InfoLabel();
        this.add(infoLabel, BorderLayout.PAGE_END);

        // add key listener
        this.addKeyListener(this);

        // set timer
        timer = new Timer(1, this);
        timer.start();

        // Create initial piece
        this.grid.createPiece();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.grid.repaint();
        if (this.counter % FREQUENCY[LEVEL-1] == 0) {
            // Let piece fall down
            if (this.grid.canMoveDown(this.grid.getBlocks())) {
                this.grid.down();
            }
            // If the piece can not move down any further, add blocks to framework
            else {
                updateGame(true);
            }
        }
        counter++;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyEvent = e.getKeyCode();
        switch (keyEvent) {
            case KeyEvent.VK_RIGHT:
                this.grid.moveRight();
                break;
            case KeyEvent.VK_LEFT:
                this.grid.moveLeft();
                break;
            case KeyEvent.VK_UP:
                this.grid.turnRight();
                break;
            case KeyEvent.VK_SPACE:
                this.grid.turnLeft();
                break;
            case KeyEvent.VK_DOWN:
                this.grid.setBlocks(this.grid.blockBottom(true));
                updateGame(false);
            default:
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    /**
     * Updates game -> add current piece to framework, check overflow, levelup.... and set next piece
     */
    public void updateGame(boolean spiel) {
        if (!spiel || this.spielCounter == SPIEL[LEVEL-1]) {
            this.grid.addBlocksToFramework();
            // Overflow -> game over
            if (this.grid.isOverflow()) {
                this.timer.stop();
                new GameOverDialog(Integer.parseInt(this.infoLabel.getScoreLabel().getScoreNum().getText()));
            }
            int deleted = grid.getFramework().deleteLines();
            // Updates score
            SCORE += deleted;
            this.infoLabel.getScoreLabel().getScoreNum().setText(Integer.toString(SCORE));
            this.spielCounter = 0;
            // Player has successed
            if (SCORE == 200) {
                this.timer.stop();
                new GameFinished();
            }
            // Increment level every 10 points
            if (SCORE >= LEVEL * 10) {
                LEVEL++;
                // Update level label
                this.infoLabel.getLevelLabel().getLevelNum().setText(Integer.toString(LEVEL));
            }
        }
        else {
            spielCounter++;
        }
    }

    public static void main(String[] args) {
        new WelcomePage();
    }
}
