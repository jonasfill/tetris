import java.util.Arrays;

public class Framework {

    // byte-matrix for storing the fixed "framework" of fixed building blocks
    private byte[][] framework;

    public Framework() {
        this.framework = new byte[Grid.getWIDTH() / Grid.getBlockWidth()][Grid.getHEIGHT() / Grid.getBlockWidth() + 7];
        // Fill matrix with zeros
        for (byte[] bytes : this.framework) {
            Arrays.fill(bytes, (byte) 0);
        }
    }

    public byte[][] getFramework() {
        return framework;
    }

    public void setFramework(byte[][] framework) {
        this.framework = framework;
    }

    /**
     * Deletes full lines and returns the number of lines deleted
     * @return
     */
    public int deleteLines() {
        // Keeps track of offset which is applied to lines following deleted lines
        int offset = 0;
        for (int i = 0; i < framework[0].length; i++) {
            boolean toDelete = true;
            for (int j = 0; j < framework.length; j++) {
                if (framework[j][i] == 0) toDelete = false;
                else {
                    // Apply offset
                    framework[j][i] = 0; framework[j][i-offset] = 1;
                }
            }
            if (toDelete) {
                // Delete line
                for (int j = 0; j < framework.length; j++) {
                    framework[j][i-offset] = 0;
                }
                offset++;
            }
        }
        return offset;
    }
}
