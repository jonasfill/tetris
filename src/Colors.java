import java.awt.*;

public class Colors {
    public static Color COLOR_FRAMEWORK = new Color(0x996600);

    // Color for bar
    public static Color COLOR_BAR = new Color(0xFFFF00);

    // Color for square
    public static Color COLOR_SQUARE = new Color(0x4169E1);

    // Color for dromedar
    public static Color COLOR_DROMEDAR = new Color(0xFF0033);

    // Color for skew
    public static Color COLOR_SKEW = new Color(0x228B22);

    // Color for lshape
    public static Color COLOR_LSHAPE = new Color(0xFF00FF);

    public static Color COLOR_TEMPLATE = Color.gray;
}
