import javax.swing.*;
import java.awt.*;

public class ScoreLabel extends JPanel {

    private JLabel scoreLabel;

    private JLabel scoreNum;

    public ScoreLabel() {
        // add components
        scoreLabel = new JLabel("Score:");
        this.add(scoreLabel, BorderLayout.LINE_START);
        scoreNum = new JLabel("0");
        this.add(scoreNum, BorderLayout.LINE_END);
    }

    public JLabel getScoreLabel() {
        return scoreLabel;
    }

    public void setScoreLabel(JLabel scoreLabel) {
        this.scoreLabel = scoreLabel;
    }

    public JLabel getScoreNum() {
        return scoreNum;
    }

    public void setScoreNum(JLabel scoreNum) {
        this.scoreNum = scoreNum;
    }
}
