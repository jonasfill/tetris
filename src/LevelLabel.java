import javax.swing.*;
import java.awt.*;

public class LevelLabel extends JPanel {

    private JLabel levelLabel = new JLabel("Level");

    private JLabel levelNum = new JLabel("1");


    public LevelLabel() {
        // add components
        levelLabel = new JLabel("Level");
        this.add(levelLabel, BorderLayout.LINE_START);
        levelNum = new JLabel("1");
        this.add(levelNum, BorderLayout.LINE_END);
    }

    public JLabel getLevelLabel() {
        return levelLabel;
    }

    public void setLevelLabel(JLabel levelLabel) {
        this.levelLabel = levelLabel;
    }

    public JLabel getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(JLabel levelNum) {
        this.levelNum = levelNum;
    }
}
