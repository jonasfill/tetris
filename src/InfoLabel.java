import javax.swing.*;
import java.awt.*;

public class InfoLabel extends JPanel {
    private ScoreLabel scoreLabel;

    private LevelLabel levelLabel;
    public InfoLabel() {
        scoreLabel = new ScoreLabel();
        this.add(scoreLabel, BorderLayout.LINE_START);
        levelLabel = new LevelLabel();
        this.add(levelLabel, BorderLayout.LINE_END);
    }

    public ScoreLabel getScoreLabel() {
        return scoreLabel;
    }

    public void setScoreLabel(ScoreLabel scoreLabel) {
        this.scoreLabel = scoreLabel;
    }

    public LevelLabel getLevelLabel() {
        return levelLabel;
    }

    public void setLevelLabel(LevelLabel levelLabel) {
        this.levelLabel = levelLabel;
    }
}
