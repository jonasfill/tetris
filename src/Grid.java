import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Grid extends JPanel {

    // Dimensions
    private static int WIDTH = 400;

    private static int HEIGHT = 800;

    private static int BLOCK_WIDTH = 40;

    private Framework framework;

    // Stores piece currently in play
    private Piece piece;

    // Blocks belonging to current piece
    private ArrayList<Point> blocks;

    // Variables indication if turns are possible
    private boolean canTurnRight;

    private boolean canTurnLeft;

    // Does a block overflow the grid
    private boolean overflow;

    // set paddings
    int paddingX = 10;
    int paddingY = 10;

    public Grid() {
        framework = new Framework();
        blocks = new ArrayList<>();
    }

    public void createPiece() {
        // Create new piece
        this.setPiece(new Piece());
        // Set initial possible turns
        this.turnsPossible();
        // Set blocks belonging to piece
        this.blocks = this.setBlocks(this.piece);
    }

    public void paintComponent(Graphics g) {
        // Color background grid
        g.setColor(Color.BLACK);
        g.fillRect(paddingX, paddingY, WIDTH, HEIGHT);

        // Color framework
        int startY = 760;
        for (int i = 0; i < framework.getFramework().length; i++) {
            for (int j = 0; j < framework.getFramework()[i].length; j++) {
                if (framework.getFramework()[i][j] == 1) {
                    g.setColor(Colors.COLOR_FRAMEWORK);
                    g.fill3DRect(paddingX + i * BLOCK_WIDTH,(startY + paddingY) - j * BLOCK_WIDTH,
                            BLOCK_WIDTH, BLOCK_WIDTH, true);
                }
            }
        }

        // Color template
        ArrayList<Point> bottom = this.blockBottom(false);
        for (Point p : bottom) {
            g.setColor(Colors.COLOR_TEMPLATE);
            g.fillRect(paddingX + p.x * BLOCK_WIDTH, paddingY + ((HEIGHT / BLOCK_WIDTH - 1) - p.y) * BLOCK_WIDTH,
                    BLOCK_WIDTH, BLOCK_WIDTH);
        }

        // Color pieces
        if (piece.getPieceType() == PieceType.SQUARE) {
            g.setColor(Colors.COLOR_SQUARE);
        }
        else if (piece.getPieceType() == PieceType.DROMEDAR) {
            g.setColor(Colors.COLOR_DROMEDAR);

        }
        else if (piece.getPieceType() == PieceType.SKEW_RIGHT || piece.getPieceType() == PieceType.SKEW_LEFT) {
            g.setColor(Colors.COLOR_SKEW);

        }
        else if (piece.getPieceType() == PieceType.LSHAPE_RIGHT || piece.getPieceType() == PieceType.LSHAPE_LEFT) {
            g.setColor(Colors.COLOR_LSHAPE);
        }
        else if (piece.getPieceType() == PieceType.BAR) {
            g.setColor(Colors.COLOR_BAR);
        }
        for (Point p : blocks) {
            g.fill3DRect(paddingX + p.x * BLOCK_WIDTH, paddingY + ((HEIGHT / BLOCK_WIDTH - 1) - p.y) * BLOCK_WIDTH,
                    BLOCK_WIDTH, BLOCK_WIDTH, true);
        }

    }

    public Framework getFramework() {
        return framework;
    }

    public void setFramework(Framework framework) {
        this.framework = framework;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
        this.piece.setPieceAlignment(PieceAlignment.VERTICAL_RIGHT);
        this.blocks = this.setBlocks(this.piece);
    }

    public int getPaddingX() {
        return paddingX;
    }

    public void setPaddingX(int paddingX) {
        this.paddingX = paddingX;
    }

    public int getPaddingY() {
        return paddingY;
    }

    public void setPaddingY(int paddingY) {
        this.paddingY = paddingY;
    }

    public ArrayList<Point> getBlocks() {
        return blocks;
    }

    public void setBlocks(ArrayList<Point> blocks) {
        this.blocks = blocks;
    }

    public boolean isOverflow() {
        return overflow;
    }

    public void setOverflow(boolean overflow) {
        this.overflow = overflow;
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static void setWIDTH(int WIDTH) {
        Grid.WIDTH = WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public static void setHEIGHT(int HEIGHT) {
        Grid.HEIGHT = HEIGHT;
    }

    public static int getBlockWidth() {
        return BLOCK_WIDTH;
    }

    public static void setBlockWidth(int blockWidth) {
        BLOCK_WIDTH = blockWidth;
    }

    public ArrayList<Point> setBlocks(Piece piece) {
        ArrayList<Point> blocks = new ArrayList<>();
        if (piece.getPieceType() == PieceType.SQUARE) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            blocks.add(new Point((piece.getPosition().x + 1),

                    piece.getPosition().y));
            blocks.add(new Point(piece.getPosition().x,

                    (piece.getPosition().y - 1)));
            blocks.add(new Point((piece.getPosition().x + 1),

                    (piece.getPosition().y - 1)));
        }
        else if (piece.getPieceType() == PieceType.DROMEDAR) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP) {
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
            }
        }
        else if (piece.getPieceType() == PieceType.SKEW_RIGHT) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x - 1),

                        (piece.getPosition().y - 1)));

            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x + 1),

                        (piece.getPosition().y - 1)));

            }
        }
        else if (piece.getPieceType() == PieceType.SKEW_LEFT) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        (piece.getPosition().y - 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                blocks.add(new Point(piece.getPosition().x + 1,

                        (piece.getPosition().y)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y + 1));
                blocks.add(new Point((piece.getPosition().x),

                        (piece.getPosition().y - 1)));
            }
        }
        else if (piece.getPieceType() == PieceType.LSHAPE_LEFT) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP) {
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x - 1),

                        (piece.getPosition().y + 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x + 1),

                        (piece.getPosition().y - 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        (piece.getPosition().y + 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x - 1),

                        (piece.getPosition().y - 1)));
            }
        }
        else if (piece.getPieceType() == PieceType.LSHAPE_RIGHT) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP) {
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x + 1),

                        (piece.getPosition().y + 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x - 1),

                        (piece.getPosition().y - 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x + 1),

                        (piece.getPosition().y - 1)));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
                blocks.add(new Point((piece.getPosition().x - 1),

                        (piece.getPosition().y + 1)));
            }
        }
        else if (piece.getPieceType() == PieceType.BAR) {
            blocks.add(new Point(piece.getPosition().x,

                    piece.getPosition().y));
            if (piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                blocks.add(new Point((piece.getPosition().x - 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x + 1),

                        piece.getPosition().y));
                blocks.add(new Point((piece.getPosition().x + 2),

                        piece.getPosition().y));
            }
            else if (piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 2)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y + 1)));
                blocks.add(new Point(piece.getPosition().x,

                        (piece.getPosition().y - 1)));
            }
        }
        return blocks;
    }

    public void moveRight() {
        Point new_pos = new Point(this.piece.getPosition().x + 1, this.piece.getPosition().y);
        // If the piece can be moved to the right, move it
        if (this.canMoveRight()) {
            this.piece.getPosition().x++;
        }
        // Update possible turns
        this.turnsPossible();
        // Update blocks
        this.blocks = this.setBlocks(this.piece);
    }

    public void moveLeft() {
        //Point new_pos = new Point(this.piece.getPosition().x - 1, this.piece.getPosition().y);
        // If the piece can be moved to the right, move it
        if (this.canMoveLeft()) {
            this.piece.getPosition().x--;
        }
        // Update possible turns
        this.turnsPossible();
        // Update blocks
        this.blocks = this.setBlocks(this.piece);
    }

    public void turnRight() {
        if (this.canTurnRight) {
            if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP) {
                this.piece.setPieceAlignment(PieceAlignment.VERTICAL_RIGHT);
            } else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                this.piece.setPieceAlignment(PieceAlignment.HORIZONTAL_DOWN);
            } else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                this.piece.setPieceAlignment(PieceAlignment.VERTICAL_LEFT);
            } else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                this.piece.setPieceAlignment(PieceAlignment.HORIZONTAL_UP);
            }
        }
        ArrayList<Point> newBlocks = this.setBlocks(this.piece);
        this.adjustPiecePosition(newBlocks);
        // Update blocks
        this.blocks = newBlocks;
        // Update possible turns for current alignment
        this.turnsPossible();
    }

    public void turnLeft() {
        if (this.canTurnLeft) {
            if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP) {
                this.piece.setPieceAlignment(PieceAlignment.VERTICAL_LEFT);
            } else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                this.piece.setPieceAlignment(PieceAlignment.HORIZONTAL_DOWN);
            } else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                this.piece.setPieceAlignment(PieceAlignment.VERTICAL_RIGHT);
            } else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                this.piece.setPieceAlignment(PieceAlignment.HORIZONTAL_UP);
            }
        }
        // Update possible turns for current alignment
        this.turnsPossible();
        // Update blocks
        this.blocks = this.setBlocks(this.piece);
    }

    /**
     * Determines which turns are possible in current alignment
     */
    public void turnsPossible() {
        if (this.piece.getPieceType() == PieceType.DROMEDAR || this.piece.getPieceType() == PieceType.LSHAPE_RIGHT ||
            this.piece.getPieceType() == PieceType.LSHAPE_LEFT) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                if (this.piece.getPosition().x >= 1) {
                    this.canTurnRight = true;
                    this.canTurnLeft = true;
                } else {
                    this.canTurnRight = false;
                    this.canTurnLeft = false;
                }
            } else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                if (this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2) {
                    this.canTurnRight = true;
                    this.canTurnLeft = true;
                } else {
                    this.canTurnRight = false;
                    this.canTurnLeft = false;
                }
            } else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                this.canTurnRight = true;
                this.canTurnLeft = true;
            }
        }
        else if (this.piece.getPieceType() == PieceType.SKEW_RIGHT || this.piece.getPieceType() == PieceType.SKEW_LEFT) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                if (this.piece.getPosition().x >= 1) {
                    this.canTurnRight = true;
                    this.canTurnLeft = true;
                } else {
                    this.canTurnRight = false;
                    this.canTurnLeft = false;
                }
            } else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                this.canTurnRight = true;
                this.canTurnLeft = true;
            }
        } else if (this.piece.getPieceType() == PieceType.SQUARE) {
            this.canTurnRight = true;
            this.canTurnLeft = true;
        } else if (this.piece.getPieceType() == PieceType.BAR) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                if (this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3) {
                    this.canTurnRight = true;
                    this.canTurnLeft = true;
                } else {
                    this.canTurnRight = false;
                    this.canTurnLeft = false;
                }
            } else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                this.canTurnRight = true;
                this.canTurnLeft = true;
            }
        }
    }

    /**
     * Determine if the piece can move to the right
     * @return
     */
    public boolean canMoveRight() {
        /*
        if (this.piece.getPieceType() == PieceType.DROMEDAR || this.piece.getPieceType() == PieceType.LSHAPE) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                return this.piece.getPosition().x >= 0 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3;
            }
        }
        if (this.piece.getPieceType() == PieceType.SKEW) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                return this.piece.getPosition().x >= 0 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3;
            }
        }
        else if (this.piece.getPieceType() == PieceType.SQUARE) {
            return this.piece.getPosition().x >= 0 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3;
        }
        else if (this.piece.getPieceType() == PieceType.BAR) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                return this.piece.getPosition().x >= 0 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 4;
            }
        }
        return fals

         */
        for (Point p : this.blocks) {
            if (p.x == WIDTH / BLOCK_WIDTH - 1 || this.framework.getFramework()[p.x + 1][p.y] == 1) {
                return false;
            }
        }
        return true;
    }
    /**
     * Determine if the piece can move to the left
     * @return
     */
    public boolean canMoveLeft() {
        /*
        if (this.piece.getPieceType() == PieceType.DROMEDAR || this.piece.getPieceType() == PieceType.LSHAPE) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 1;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                return this.piece.getPosition().x >= 2 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 1;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                return this.piece.getPosition().x >= 2 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2;
            }
        }
        if (this.piece.getPieceType() == PieceType.SKEW) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                return this.piece.getPosition().x >= 2 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2;
            }
        }
        else if (this.piece.getPieceType() == PieceType.SQUARE) {
            return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 2;
        }
        else if (this.piece.getPieceType() == PieceType.BAR) {
            if (this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_RIGHT ||
                    this.piece.getPieceAlignment() == PieceAlignment.VERTICAL_LEFT) {
                return this.piece.getPosition().x >= 1 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 1;
            }
            else if (this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_UP ||
                    this.piece.getPieceAlignment() == PieceAlignment.HORIZONTAL_DOWN) {
                return this.piece.getPosition().x >= 2 && this.piece.getPosition().x <= WIDTH / BLOCK_WIDTH - 3;
            }
        }
        return false;
        */
        for (Point p : this.blocks) {
            if (p.x == 0 || this.framework.getFramework()[p.x - 1][p.y] == 1) {
                return false;
            }
        }
        return true;
    }

    public void down() {
        this.piece.down();
        this.blocks = this.setBlocks(this.piece);
    }

    /**
     * Get blocks of current piece on bottom position
     * @return
     */
    public ArrayList<Point> blockBottom(boolean updatePiece) {
        // Initialize temporary piece
        Piece tmpPiece;
        if (updatePiece) {
            tmpPiece = this.piece;
        }
        else {
            tmpPiece = new Piece(this.piece.getPieceType(), this.piece.getPieceAlignment(),
                    new Point(this.piece.getPosition().x, this.piece.getPosition().y));
        }
        // Initialize block list
        ArrayList<Point> tmpBlocks = new ArrayList<>();
        // Add blocks to new list
        for (Point p : this.blocks) {
            tmpBlocks.add(new Point(p.x, p.y));
        }
        while (canMoveDown(tmpBlocks)) {
            //this.printBlocks(tmpBlocks);
            tmpPiece.down();
            tmpBlocks = this.setBlocks(tmpPiece);
        }
        return tmpBlocks;
    }

    /**
     * Returns true if the piece can move down further
     * @return
     */
    public boolean canMoveDown(ArrayList<Point> blocks) {
        for (Point p : blocks) {
            // Does block touch the ground?
            if (p.y == 0) {
                return false;
            }
            //System.out.println("(" + p.x + ", " + p.y + ")");
            // Does block touch framework?
            if (framework.getFramework()[p.x][p.y-1] == 1) {
                return false;
            }
        }
        // No block touches anything
        return true;
    }

    /**
     * Add blocks of current piece to framework
     */
    public void addBlocksToFramework() {
        // Add to framework
        for (Point p : this.blocks) {
            this.framework.getFramework()[p.x][p.y] = 1;
            // Set overflow if necessary
            if (p.y >= HEIGHT / BLOCK_WIDTH) {
                this.overflow = true;
            }
        }
        // Clear blocks list
        this.blocks.clear();
        // Create new piece
        this.piece = new Piece();
    }

    /**
     * Adjust position of piece in case it "underflows" the ground
     */
    public ArrayList<Point> adjustPiecePosition(ArrayList<Point> blocks) {
        boolean toAdjust = false;
        for (Point p : blocks) {
            if (p.y < 0) {
                toAdjust = true;
                break;
            }
        }
        if (toAdjust) {
            for (Point p : blocks) {
                // Move piece one block up
                p.x++; p.y++;
            }
        }
        return blocks;
    }

    private void printBlocks(ArrayList<Point> blocks) {
        String string = "";
        for (Point p : blocks) {
            string += "(" + p.x + "," + p.y + ")";
        }
        System.out.println(string);
    }
}
